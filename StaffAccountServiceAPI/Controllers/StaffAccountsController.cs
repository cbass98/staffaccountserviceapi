﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StaffAccountServiceAPI.Data;
using StaffAccountServiceAPI.Models;

namespace StaffAccountServiceAPI.Controllers
{
    [Route("api/staffaccounts")]
    [ApiController]
    public class StaffAccountsController : ControllerBase
    {
        private readonly StaffAccountContext _context;

        public StaffAccountsController(StaffAccountContext context)
        {
            _context = context;
        }

        // GET: api/StaffAccounts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StaffAccount>>> GetStaffAccount()
        {
            return await _context.StaffAccount.ToListAsync();
        }

        // GET: api/StaffAccounts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StaffAccount>> GetStaffAccount(long id)
        {
            var staffAccount = await _context.StaffAccount.FindAsync(id);

            if (staffAccount == null)
            {
                return NotFound();
            }

            return staffAccount;
        }

        // PUT: api/StaffAccounts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStaffAccount(long id, StaffAccount staffAccount)
        {
            if (id != staffAccount.Id)
            {
                return BadRequest();
            }

            _context.Entry(staffAccount).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StaffAccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/StaffAccounts
        [HttpPost]
        public async Task<ActionResult<StaffAccount>> PostStaffAccount(StaffAccount staffAccount)
        {
            _context.StaffAccount.Add(staffAccount);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetStaffAccount", new { id = staffAccount.Id }, staffAccount);
        }

        // DELETE: api/StaffAccounts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<StaffAccount>> DeleteStaffAccount(long id)
        {
            var staffAccount = await _context.StaffAccount.FindAsync(id);
            if (staffAccount == null)
            {
                return NotFound();
            }

            _context.StaffAccount.Remove(staffAccount);
            await _context.SaveChangesAsync();

            return staffAccount;
        }

        private bool StaffAccountExists(long id)
        {
            return _context.StaffAccount.Any(e => e.Id == id);
        }
    }
}
