﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using StaffAccountServiceAPI.Data;

namespace StaffAccountServiceAPI.Migrations
{
    [DbContext(typeof(StaffAccountContext))]
    [Migration("20191125120319_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("StaffAccountServiceAPI.Models.StaffAccount", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address");

                    b.Property<string>("Email");

                    b.Property<bool>("IsComplete");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<string>("Postcode");

                    b.Property<string>("Telephone");

                    b.HasKey("Id");

                    b.ToTable("StaffAccount");
                });
#pragma warning restore 612, 618
        }
    }
}
