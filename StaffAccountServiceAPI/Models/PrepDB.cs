﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StaffAccountServiceAPI.Data;

namespace StaffAccountServiceAPI.Models
{
    public class PrepDB
    {
        public static void PrepPopulation(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<StaffAccountContext>());
            }
        }
        public static void SeedData(StaffAccountContext context)
        {
            System.Console.WriteLine("Applying Migrations...");

            context.Database.Migrate();
        }
    }
}
