﻿using System.ComponentModel.DataAnnotations;

namespace StaffAccountServiceAPI.Models
{
    public class StaffAccount
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public bool IsManagement { get; set; }
    }
}
