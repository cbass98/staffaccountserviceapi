﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StaffAccountServiceAPI.Models;

namespace StaffAccountServiceAPI.Data
{
    public class StaffAccountContext : DbContext
    {
        public StaffAccountContext (DbContextOptions<StaffAccountContext> options)
            : base(options)
        {
        }

        public DbSet<StaffAccountServiceAPI.Models.StaffAccount> StaffAccount { get; set; }
    }
}
